﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class Barista : FoodWorker
    {
        public override FoodDrink PrepareFood()
        {
            Coffee mochaCharge = new Coffee();
            mochaCharge.brewStyle = "hot";
            mochaCharge.caloriesPerServing = 650;
            mochaCharge.name = "Moche freakin' Charge";
            mochaCharge.isCaffeinated = true;

            AskNicelyForTips();

            return mochaCharge;
        }
    }
}
