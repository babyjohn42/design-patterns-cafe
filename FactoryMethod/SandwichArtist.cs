﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class SandwichArtist : FoodWorker
    {
        public override FoodDrink PrepareFood()
        {
            Sandwich sub = new Sandwich();
            sub.caloriesPerServing = 1200;
            sub.hasCheese = true;
            sub.name = "Seafood Sensation";
            sub.isCaffeinated = false;
            sub.breadType = "Whole grain wheat";

            return sub;
        }
    }
}
