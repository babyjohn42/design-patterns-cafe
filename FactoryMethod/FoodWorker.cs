﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    abstract class FoodWorker
    {
        public abstract FoodDrink PrepareFood();
        public void AskNicelyForTips()
        {
            Console.WriteLine("If you could just sign the top copy for me, the bottom copy is yours! Have a great day!!! :) ");
        }
    }
}
