﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    abstract class FoodDrink
    {
        public double caloriesPerServing;
        public string name;
        public bool isCaffeinated;
    }
}
