﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            List<FoodWorker> cafeEmployees = new List<FoodWorker>();

            FoodWorker ollie = new Barista();
            FoodWorker collin = new SandwichArtist();
            FoodWorker ryo = new SandwichArtist();
            FoodWorker genevieve = new Barista();
            FoodWorker hans = new Barista();

            cafeEmployees.Add(ollie);
            cafeEmployees.Add(collin);
            cafeEmployees.Add(ryo);
            cafeEmployees.Add(genevieve);
            cafeEmployees.Add(hans);

            foreach(FoodWorker employee in cafeEmployees)
            {
                FoodDrink menuItem = employee.PrepareFood();
                Console.WriteLine($"Here is your {menuItem.name}!");
            }

            Console.ReadLine();
        }
    }
}
