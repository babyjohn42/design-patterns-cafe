﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class MenuSingleton
    {
        private static MenuSingleton instance;

        private Dictionary<string, decimal> foodChoices;

        public static MenuSingleton GetInstance()
        {
            if (instance == null)
            {
                instance = new MenuSingleton();
            }

            return instance;
        }

        private MenuSingleton()
        {
            foodChoices = new Dictionary<string, decimal>()
            {
                { "Banana Bread", 3.00M },
                { "Pineapple chicken rice", 11.19M },
                { "Nachos", 0.00M }
            };
        }

        public void DisplayMenu()
        {
            foreach( var foodChoice in foodChoices)
            {
                Console.WriteLine($"{foodChoice.Key} - {foodChoice.Value.ToString("C2")}");
            }
        }
    }
}
