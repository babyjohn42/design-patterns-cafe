﻿using System;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            MenuSingleton menu = MenuSingleton.GetInstance();

            menu.DisplayMenu();

            MenuSingleton menu2 = MenuSingleton.GetInstance();

            menu2.DisplayMenu();

            Console.WriteLine(menu == menu2);

            Console.ReadLine();
        }
    }
}
