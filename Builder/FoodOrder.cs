﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class FoodOrder
    {
        // dependency injection
        private ICanCook builder;

        // director
        public bool wantsTurkey;
        public bool wantsDessert;
        public bool wantsVeggies;
        public bool wantsPotatoes;

        // dependency injection
        public FoodOrder(ICanCook canCook)
        {
            builder = canCook;
        }

        // director
        public void ConstructPlate()
        {
            if (wantsTurkey)
            {
                builder.BuildTurkey();
            }

            if (wantsPotatoes)
            {
                builder.BuildPotatoes();
            }

            if (wantsVeggies)
            {
                builder.BuildVeggies();
            }

            if (wantsDessert)
            {
                builder.BuildDessert();
            }
        }
    }
}
