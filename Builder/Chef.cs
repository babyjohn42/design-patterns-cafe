﻿using System;

namespace Builder
{
    class Chef : ICanCook
    {
        private Plate plate = new Plate();

        public Plate GetPlate()
        {
            return plate;
        }

        public void BuildDessert()
        {
            plate.foodItems.Add("Creme brulee");
            plate.foodItems.Add("Caramel");
        }

        public void BuildPotatoes()
        {
            plate.foodItems.Add("Potatoes au gratin");
        }

        public void BuildTurkey()
        {
            plate.foodItems.Add("Stuffed turkey au jus");
        }

        public void BuildVeggies()
        {
            plate.foodItems.Add("Grilled asparagus");
        }
    }
}
