﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class Mom : ICanCook
    {
        private Plate plate = new Plate();

        public Plate GetPlate()
        {
            // writing a comment in real-time
            return plate;
        }

        public void BuildDessert()
        {
            plate.foodItems.Add("Apple cobbler");
        }

        public void BuildPotatoes()
        {
            plate.foodItems.Add("Mashed potatoes");
            plate.foodItems.Add("Butter");
        }

        public void BuildTurkey()
        {
            plate.foodItems.Add("Deli meat turkey from Meijer");
        }

        public void BuildVeggies()
        {
            return;
        }
    }
}
