﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class Plate
    {
        public List<string> foodItems = new List<string>();

        public override string ToString()
        {
            string result = "";
            foreach(string food in foodItems)
            {
                result += food + Environment.NewLine;
            }

            return result;
        }
    }
}
