﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            Mom cook = new Mom();
            FoodOrder myWay = new FoodOrder(cook);

            myWay.wantsVeggies = true;
            myWay.wantsTurkey = true;
            myWay.wantsPotatoes = true;

            myWay.ConstructPlate();
            Plate myPlate = cook.GetPlate();

            Console.WriteLine(myPlate);
            Console.ReadLine();
        }
    }
}
